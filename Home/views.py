from django.shortcuts import render
from .models import About_me,Skills_me

def home(request):
    about_me = About_me.objects.all()
    skill_me = Skills_me.objects.all()
    return render(request, 'index.html',{'about_me':about_me,'skill_me':skill_me})