# Generated by Django 3.1.2 on 2020-11-01 00:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Home', '0007_auto_20201101_0331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='skills_me',
            name='AJAX',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='skills_me',
            name='Django',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='skills_me',
            name='HTML',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='skills_me',
            name='PHP',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='skills_me',
            name='jQuery',
            field=models.IntegerField(blank=True, default=0),
        ),
    ]
