# Generated by Django 3.1.2 on 2020-11-01 13:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Home', '0010_auto_20201101_1632'),
    ]

    operations = [
        migrations.RenameField(
            model_name='skills_me',
            old_name='skiil_name',
            new_name='skill_name',
        ),
    ]
