from django.db import models

class About_me(models.Model):
    name = models.CharField(max_length=100)
    job = models.CharField(max_length=100,blank=True)
    dob = models.IntegerField(default=0)
    phone = models.IntegerField(default='1234567890')
    address = models.CharField(max_length=200)
    email = models.EmailField()
    website = models.CharField(max_length=300)
    about = models.TextField()
    web_project = models.IntegerField(default=0)
    django_project = models.IntegerField(default=0)
    php_project = models.IntegerField(default=0)
    slogan = models.CharField(max_length=300)
    about_slogan = models.TextField()
    profile = models.ImageField(upload_to='images/',blank=True)
class Skills_me(models.Model):
    skill_name = models.CharField(max_length=100, default="")
    skill_percentage = models.IntegerField(default=0)
